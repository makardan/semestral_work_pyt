import os
import time

import json
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import uvicorn
import websockets
from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from scrapper.scrapper import download_data, listdir

app = FastAPI()

templates = Jinja2Templates(directory='templates')
app.mount('/static', StaticFiles(directory='static'), name='static')


@app.get('/')
def get_main(request: Request):
    """ Function returns main html """
    return templates.TemplateResponse('main.html', {'request': request})


@app.get('/analytics', response_class=HTMLResponse)
async def get_index(request: Request):
    """ Route to /analytics """
    list_of_currencies = listdir(os.path.join(os.getcwd(), 'datasets'))

    if list_of_currencies:
        list_of_currencies.sort()
        return templates.TemplateResponse('index.html', {"request": request, "currencies": list_of_currencies})

    return templates.TemplateResponse('index.html', {"request": request})


@app.get('/get_data', response_class=HTMLResponse)
async def get_data():
    """ Gets data from a website """
    list_of_currencies = listdir(os.path.join(os.getcwd(), 'datasets'))

    if not list_of_currencies:
        await download_data()

    return RedirectResponse('/analytics')


@app.post('/analytics')
async def show_graph(request: Request, first_currency: str = Form(), second_currency: str = Form(),
                     field: str = Form()):
    """ Fuction shows matplotlib graph with user parameters """
    dataset1 = read_csv(first_currency)
    dataset2 = read_csv(second_currency)

    dates1 = np.array(dataset1['Date'])
    dates2 = np.array(dataset2['Date'])

    dataset1_array = np.array(dataset1[field])
    dataset2_array = np.array(dataset2[field])

    fig = plt.figure(figsize=(10, 10), facecolor='#F3F8F2')
    ax = fig.add_subplot()
    fig.suptitle(f'Difference in {field}')
    ax.set_xlabel('Date')
    ax.set_ylabel(field)

    if first_currency == second_currency:
        ax.plot(dates1, dataset1_array, 'r-o', label=first_currency[:-4])
    else:
        ax.plot(dates1, dataset1_array, 'r-o', label=first_currency[:-4])
        ax.plot(dates2, dataset2_array, 'm-s', label=second_currency[:-4])

    ax.legend()
    plt.minorticks_on()
    plt.grid(which='major', color='#444', linewidth=1)
    plt.grid(which='minor', color='#aaa', linewidth=0.5)
    plt.show()

    list_of_currencies = listdir(os.path.join(os.getcwd(), 'datasets'))
    list_of_currencies.sort()

    return templates.TemplateResponse('index.html', {"request": request, "currencies": list_of_currencies})


def read_csv(filename: str):
    """ Just reading csv """
    return pd.read_csv(os.path.join(os.getcwd(), 'datasets', filename))


@app.get('/binanceApi')
async def get_binance_page(request: Request):
    """ Redirecting to binance.html """
    return templates.TemplateResponse('binance.html', {"request": request})


@app.post('/binanceApi')
async def get_binance_data(request: Request, currency: str = Form(), count_of_records: str = Form()):
    """ Streaming from stream.binance.com """
    url = f'wss://stream.binance.com:9443/ws/{currency + "usd"}t@kline_1s'

    binance_graph = plt.figure(figsize=(10, 7), facecolor='#F3F8F2')
    binance_graph.suptitle(f'{currency.capitalize()} to USD')
    ax = binance_graph.add_subplot()
    ax.set_xlabel('Date')
    ax.set_ylabel("USD ($)")
    plt.minorticks_on()

    plt.grid(which='major', color='#444', linewidth=1)
    plt.grid(which='minor', color='#aaa', linewidth=0.5)
    binance_graph.show()

    dates = []
    prices = []

    def update_graph():
        ax.plot(dates, prices, 'm-s', label=currency)

        binance_graph.canvas.draw()
        plt.pause(0.1)

    async with websockets.connect(url) as client:
        for i in range(int(count_of_records)):
            data = json.loads(await client.recv())

            event_time = time.localtime(data['E'] // 1000)
            event_time = f'{event_time.tm_hour}:{event_time.tm_min}:{event_time.tm_sec}'

            dates.append(event_time)
            prices.append((float(data['k']['c'])))
            update_graph()

            if not plt.fignum_exists(binance_graph.number) and i != int(count_of_records) - 1:
                await client.close()
                return templates.TemplateResponse('binance.html', {"request": request})

        await client.close()
    plt.close(binance_graph)

    return templates.TemplateResponse('binance.html', {"request": request})


if __name__ == '__main__':
    uvicorn.run('main:app', port=8000, reload=True)
