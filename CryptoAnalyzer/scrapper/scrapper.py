""" Module which contains functions for downloading csv data """
import os
import time

import zipfile
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from dotenv import load_dotenv


async def download_data():
    """ Downloads data with selenium """
    load_dotenv()
    options = webdriver.ChromeOptions()
    options.add_argument(os.getenv('USER_AGENT'))
    options.add_argument("--window-size=1920,1080")
    driver_path = os.path.abspath('./chromeDriver/chromeDriver')
    driver = webdriver.Chrome(executable_path=driver_path, options=options)

    dataset_path = os.path.join(os.getcwd(), 'datasets')
    files = listdir(dataset_path)

    if files:
        for f in listdir(dataset_path):
            os.remove(os.path.join(dataset_path, f))

    driver.get('https://www.kaggle.com/datasets/odins0n/top-50-cryptocurrency-historical-prices?resource=download')

    download_button = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.XPATH, '//button')))
    download_button[10].click()

    login_button = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.XPATH, '//div')))
    login_button[20].click()

    email_input = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'email')))
    password_input = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'password')))
    email_input.send_keys(os.getenv('EMAIL'))
    password_input.send_keys(os.getenv('PASSWORD'))

    sign_in_button = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//button')))
    sign_in_button.click()

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'rmwcPortal')))
    driver.refresh()
    download_button_new = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.XPATH, '//button')))

    download_button_new[9].click()

    check_file_existing()

    with zipfile.ZipFile(f'/home/{os.getlogin()}/Downloads/archive.zip') as f:
        f.extractall(dataset_path)

    os.remove(f'/home/{os.getlogin()}/Downloads/archive.zip')

    return listdir(dataset_path)


def check_file_existing():
    """ Check file existing in /Downloads """
    while not os.path.exists(f'/home/{os.getlogin()}/Downloads/archive.zip'):
        time.sleep(1)

    if not os.path.isfile(f'/home/{os.getlogin()}/Downloads/archive.zip'):
        raise ValueError('Error downloading file')


def listdir(path):
    """ Listing the directory without invisible files """
    files = []
    for file in os.listdir(path):
        if not file.startswith('.'):
            files.append(file)
    return files


if __name__ == '__main__':
    print('Please, run script in CryptoAnalyzer/main.py')
