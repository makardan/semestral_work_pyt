import os
from dotenv import load_dotenv


def test_actual_directory():
    assert os.getcwd().split('/')[-1] == 'CryptoAnalyzer', 'Your actual directory must be CryptoAnalyzer'
    
def test_check_env_file():
    assert os.path.isfile(os.path.join(os.getcwd(), '.env')) == True, 'You must have .env file'

def test_correct_env():
    load_dotenv()
    assert os.getenv('EMAIL') == 'makardan@fit.cvut.cz'
    assert os.getenv('PASSWORD') == '1234567'
    assert os.getenv('USER_AGENT') == 'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'

def test_check_datasets_directory():
    assert os.path.isdir(os.path.join(os.getcwd(), 'datasets')) == True, 'You must have datasets directory'