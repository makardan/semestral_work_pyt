# CryptoAnalyzer

Welcome to the semestral work of Danil Makarov in BI-PYT course 2022. This is a small training project whose main goal is to make a full-stack application and work with Selenium, Matplotlib and streaming data from Binance site.

## How to run virtual environment

The first of all, you must run the virtual environment, because this program uses different modules like pandas, fastAPI, matplotlib and other.

```bash
source env/bin/activate
```

## How to run the program

You must create .env file in a CryptoAnalyzer folder.

```bash
EMAIL='makardan@fit.cvut.cz'
PASSWORD='1234567'
USER_AGENT='user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
```
Selenium requires it.

The main thing is, you must run the application in any linux distributive and have standart loading path for browser files ~/Downloads. You must always change the directory to the CryptoAnalyzer folder (cd CryptoAnalyzer), because FastAPI searches relative path for html files.

```bash
cd CryptoAnalyzer
python3 main.app
```

Then you must open any browser and in a URL field write http://127.0.0.1:8000.